package com.yancloud.android.contractclient;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Action {
	boolean httpAccess() default true;

	boolean websocketAccess() default true;

	boolean async() default false;
}
