package com.yancloud.android.contractclient.nativesclient;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class DelimiterBasedFrameEncoder extends MessageToByteEncoder<ByteBuf> {
	static byte[] delimiter = "wonbifoodie".getBytes();

	@Override
	protected void encode(ChannelHandlerContext arg0, ByteBuf in, ByteBuf out) throws Exception {
		// System.out.println("[DelimiterBasedFrameEncoder] write:" +
		// in.readableBytes());
		out.writeBytes(in);
		out.writeBytes(delimiter);
	}
}
