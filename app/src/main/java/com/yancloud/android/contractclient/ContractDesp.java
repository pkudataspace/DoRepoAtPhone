package com.yancloud.android.contractclient;

import java.util.List;

public class ContractDesp {
	public String contractID;
	public String contractName;
	public List<String> events;
	public List<FunctionDesp> exportedFunctions;
}