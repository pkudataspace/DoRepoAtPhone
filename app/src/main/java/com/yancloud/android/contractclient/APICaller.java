package com.yancloud.android.contractclient;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yancloud.android.contractclient.databaseHelper.DatabaseDao;
import com.yancloud.android.contractclient.databaseHelper.Do;
import com.yancloud.android.reflection.get.CMDManager;


import java.util.List;

public class APICaller {
    private static DatabaseDao databaseDao = DatabaseDao.getInstance();
    public static  Do getAPIDesc(String handle){
        Do doObject = databaseDao.getInfoByDoi(handle);
        return doObject;

    }

    //调用API获得API返回结果
    public static String callAPI(String input, String handle){
        return CMDManager.callDOI(handle,input,null);
    }
}
