package com.yancloud.android.contractclient.databaseHelper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v13.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import ias.deepsearch.com.helper.ui.activity.HomeActivity;

import static android.content.Context.TELEPHONY_SERVICE;

public class PhoneInfoUtil {


    //获取IMEI；
    public static String getDeviceImei(Context context) {
        String imei = "";
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // toast("需要动态获取权限");
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        } else {
            // toast("不需要动态获取权限");
            TelephonyManager tm = (TelephonyManager)context.getSystemService(TELEPHONY_SERVICE);
            imei = tm.getDeviceId();
        }
        return imei;

    }
    //获取品牌
    public static String getPhoneBrand(){

        return android.os.Build.BRAND;
    }
    //获取型号
    public static String getPhoneModel(){

        return android.os.Build.MODEL;
    }

    //获取手机所有信息
    public static Map<String,Object> getPhoneInfo(Context context) {
        Field[] fields = Build.class.getFields();
        Map<String,Object> phoneInfoMap = new HashMap<>();
        for (Field f : fields) {
            try {
                String name = f.getName();
                Object value =  f.get(name);
                phoneInfoMap.put(name,value);
//       String brand = f.get("BRAND").toString(); //Xiaomi
//       String model = f.get("MODEL").toString(); //Redmi Note 3
                Log.d("key"+name," value"+ value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }

        String imei = getDeviceImei(context);
        phoneInfoMap.put("imei",imei);
        Log.d("imei",imei);
        return phoneInfoMap;
    }
}