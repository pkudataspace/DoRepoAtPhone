package com.yancloud.android.contractclient.databaseHelper;

import android.os.IBinder;

import com.yancloud.android.contractclient.doip.HandleService;
import com.yancloud.android.manager.IAppConnector;
import com.yancloud.android.manager.PackageVersionPair;
import com.yancloud.android.reflection.get.GetMessage;
import com.yancloud.android.reflection.get.MainServer;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.Callable;

import ias.deepsearch.com.helper.service.DaemonService;

public class DoiCallable  implements Callable<String> {
    private  String pkgName;
    private  String method;
    private String desp;
    private DatabaseDao databaseDao;
    public static DoiCallable of(String pkgName,String method,String desp) {
        return new DoiCallable(pkgName,method,desp);
    }


    public DoiCallable(String pkgName,String method,String desp) {
        this.pkgName = pkgName;
        this.method = method;
        this.desp = desp;
        }
    @Override
    public String call() throws Exception {
        String doi = HandleService.RegisterAPI(this.pkgName,this.method, null);
        databaseDao = DatabaseDao.getInstance();
        databaseDao.insertDoi(doi, pkgName, method, 0, 1,desp);
        return doi;
    }
}
