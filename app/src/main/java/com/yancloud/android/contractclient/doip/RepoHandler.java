package com.yancloud.android.contractclient.doip;

import android.util.Log;

import com.yancloud.android.reflection.get.CMDManager;

import bdware.doip.server.DoServerHandler;
import bdware.doip.server.Op;
import bdware.doip.codec.DoMessage;
import bdware.doip.codec.bean.DOIPServiceInfo;
import bdware.doip.codec.bean.DoOp;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;

@ChannelHandler.Sharable
public class RepoHandler extends DoServerHandler {

    final String indexIdentifier = "doIndex";
    DOIPServiceInfo serviceInfo;

    public RepoHandler(DOIPServiceInfo info) {
        super(info);
        Log.d("DOIP","create repo handler");
        this.serviceInfo = info;
    }

    @Op(op = DoOp.Hello)
    void Hello(ChannelHandlerContext ctx, DoMessage msg) {
        Log.d("DOIP","hello operation start");
        replyDOIPServiceInfo(ctx,msg);
    }

//    @Op(op = DoOp.List)
//    void List(ChannelHandlerContext ctx, DoMessage msg) {
//
//    }

    @Op(op = DoOp.Retrieve)
    void Retrieve(ChannelHandlerContext ctx, DoMessage msg) {

    }

//    @Op(op = DoOp.Create)
//    void Create(ChannelHandlerContext ctx, DoMessage msg) {
//
//    }
//
//    @Op(op = DoOp.Update)
//    void Update(ChannelHandlerContext ctx, DoMessage msg) {
//
//    }
//
//    @Op(op = DoOp.Delete)
//    void Delete(ChannelHandlerContext ctx, DoMessage msg) {
//
//    }

//    @Op(op = DoOp.Analysis)
//    void list(ChannelHandlerContext ctx, DoMessage msg) {
//
//    }

    @Op(op = DoOp.Call)
    void Call(ChannelHandlerContext ctx, DoMessage msg) {
        Log.d("DOIP","call operation start： call DOI: " + msg.parameters.id);
        String resp = CMDManager.callDOI(msg.parameters.id,msg.body.toString(),"test");
        replyString(ctx,msg,resp);
    }

//    @Op(op = DoOp.Search)
//    void Search(ChannelHandlerContext ctx, DoMessage msg) {
//
//    }
//

}