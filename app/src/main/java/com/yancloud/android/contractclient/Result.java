package com.yancloud.android.contractclient;

public class Result {
	boolean status;
	String msg;
	String action = null;
	Object data;

	Result() {
		status = false;
		msg = null;
		data = "";
	}
}