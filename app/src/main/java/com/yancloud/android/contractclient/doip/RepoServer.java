package com.yancloud.android.contractclient.doip;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import bdware.doip.codec.bean.DoipMessageFormat;
import bdware.doip.codec.bean.ListenerInfo;
import bdware.doip.codec.HandleServiceUtils;
import bdware.doip.codec.bean.DOIPServiceInfo;
import bdware.doip.server.Repositories.DoipServer;

public class RepoServer extends Thread{

    public static DOIPServiceInfo info;
    public static String RouterHandle = "86.5000.470/doip.router";
    HandleService hs = new HandleService(HandleServiceUtils.hrRegister);
    static RepoServer instance;
    static DoipServer server;
    static final String TAG = "DOIPRepository";

    public static void startServer(){
        Log.d(TAG,"Start Repository Server!");
        System.out.println("[DOIP] Start Repository Server");
        ListenerInfo listenerInfo =
                new ListenerInfo(
                        "udp://192.168.31.245:1719",
                        "2.1",
                        DoipMessageFormat.PACKET);
        List<ListenerInfo> listeners = new ArrayList<>();
        listeners.add(listenerInfo);
        info = new DOIPServiceInfo(
                HandleService.getRepoID(),
                "86.5000.470/dou.SUPER",
                "repository",
                listeners
        );

        info.serviceName = "androidRepoTest";
        info.serviceDescription = "test Android Repository";

        server = DoipServer.createDoipServer(info);
        instance = new RepoServer();
        instance.start();
    }

    @Override
    public void run(){
        String res = HandleService.RegisterRepo(info);
        Log.d(TAG,"Repository Server Registered: "+ res);
        server.setServerHandler(new RepoHandler(info)).registerToRouter(RouterHandle).start();
    }
}
