package com.yancloud.android.reflection.get;

import android.app.Service;
import android.content.Context;
import android.os.IBinder;


import com.yancloud.android.manager.IAppConnector;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/**
 * Created by root on 8/12/17.
 */

public class ServiceFactory {
    public static Service getService(Context c){
        try{
            Class clz = Class.forName("cn.edu.pku.apiminier.Constants");
            Field f = clz.getDeclaredField("serviceName");
            String val  = (String) f.get(null);
            clz = Class.forName(val);
            Constructor constructor = clz.getDeclaredConstructor(Context.class);
            return (Service) constructor.newInstance(c);
        }catch(Throwable e){
            return new DefaultService(c);
        }
    }
}
