package ias.deepsearch.com.ias.util;

import android.view.View;

import java.lang.reflect.Field;

/**
 * Created by vector on 17/3/1.
 */

public class ViewUtil {
    //判断一个view有没有设置OnClickListener
    public static boolean hasOnClickListener(View view) {
        try {
            Class<?> viewClass = Class.forName("android.view.View");
            Class<?> listenerClass = Class.forName("android.view.View$ListenerInfo");
            Field onClickListenerField = listenerClass.getField("mOnClickListener");
            onClickListenerField.setAccessible(true);
            Field listenerfield = viewClass.getDeclaredField("mListenerInfo");
            listenerfield.setAccessible(true);
            Object mlistenerInfo = listenerfield.get(view);
            if (mlistenerInfo == null) {
            } else {
                Object listener = onClickListenerField.get(mlistenerInfo);
                if (listener != null)
                    return true;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }
}
