package ias.deepsearch.com.helper.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.yancloud.android.contractclient.databaseHelper.DatabaseDao;
import com.yancloud.android.contractclient.databaseHelper.Do;
import com.yancloud.android.manager.IAPIBinder;
import com.yancloud.android.reflection.get.CMDManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ias.deepsearch.com.helper.R;
import ias.deepsearch.com.helper.ui.activity.HomeActivity;
import ias.deepsearch.com.helper.ui.fragment.DoiDetailFrament;
import ias.deepsearch.com.helper.ui.view.SlideButton;

public class MySimpleAdapter extends SimpleAdapter {
    HomeActivity mActivity=null;
    List<Map<String, Object>> list=new ArrayList<>();
    private DatabaseDao databaseDao;
    private String info;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = super.getView(position, convertView, parent);
        final TextView textView= (TextView) v.findViewById(R.id.textView2);
        final SlideButton btn=(SlideButton) v.findViewById(R.id.item_SwitchView);
        final TextView textView1= (TextView) v.findViewById(R.id.textView3);
        info = (String) textView.getText();
        for(int i=0;i<list.size();i++)
        {
           if(list.get(i).get("link").equals(textView.getText()))
            {
                if(String.valueOf(list.get(i).get("button")).equals("1"))
                {
                    btn.setChecked(true);
                }
                else
                    btn.setChecked(false);
            }
        }
        DatabaseDao data = DatabaseDao.getInstance();
        Do doObject1= data.getInfoByDoi(info);
        Pattern p = Pattern.compile("\\s*|\t|\r|\n");
        Matcher m = p.matcher(doObject1.getPackageName());
        String temp1 = m.replaceAll("");
        m = p.matcher(doObject1.getMethodName());
        String temp2=m.replaceAll("");
        String temp=temp1+"/"+temp2;
        textView.setText(temp);
        String apiDetail="";
        if(doObject1.getPackageName().contains("starbucks"))
        {
            apiDetail+="星巴克";
            if(doObject1.getMethodName().contains("version"))
                apiDetail+="版本信息";
            if(doObject1.getMethodName().contains("postVideoCommentLike"))
                apiDetail+="点赞评论";
            if(doObject1.getMethodName().contains("exit"))
                apiDetail+="停止运行";
            if(doObject1.getMethodName().contains("getMethodDescriptions"))
                apiDetail+="列举API信息";
            if(doObject1.getMethodName().contains("currentContent"))
                apiDetail+="获取当前内容";
        }
        if(doObject1.getPackageName().contains("aweme"))
        {
            apiDetail+="抖音";
            if(doObject1.getMethodName().contains("version"))
                apiDetail+="版本信息";
            if(doObject1.getMethodName().contains("postVideoCommentLike"))
                apiDetail+="点赞评论";
            if(doObject1.getMethodName().contains("exit"))
                apiDetail+="停止运行";
            if(doObject1.getMethodName().contains("getMethodDescriptions"))
                apiDetail+="列举API信息";
            if(doObject1.getMethodName().contains("searchById"))
                apiDetail+="根据ID获取视频";

        }
        textView1.setText(apiDetail);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                databaseDao = DatabaseDao.getInstance(mActivity);
                if(btn.isChecked==true) {
                    Log.d("状态","改变状态*****************"+(String) textView.getText());
                    databaseDao.change((String) textView.getText(), 1);

                } else {
                    databaseDao.change((String) textView.getText(), 0);
                }
                // TODO Auto-generated method stub


            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> metaData = new ArrayList<>();
//                Bundle bundle = new Bundle();
//                bundle.putString("doi", info);
//                DoiDetailFrament doiDetailFrament = new DoiDetailFrament();
//                doiDetailFrament.setArguments(bundle);
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

                View view = View.inflate(mActivity, R.layout.doi_show, null);
                TextView doiName = (TextView) view.findViewById(R.id.doi_name);
                TextView packageName = (TextView)view.findViewById(R.id.packageNmae);
                TextView methodName = (TextView)view.findViewById(R.id.methodName);
                TextView callNumber = (TextView)view.findViewById(R.id.callNumber);
                TextView desp = (TextView)view.findViewById(R.id.methodDesp);
                final TextView invokeResult = (TextView)view.findViewById(R.id.result);
                Button invoke = (Button) view.findViewById(R.id.invoke);
                DatabaseDao da = DatabaseDao.getInstance();
                String []strings=((String) textView.getText()).split("/");
                final Do doObject= da.getInfoByPkgAndMethod(strings[0],strings[1]);
                doiName.setText(doObject.getDoi());
                packageName.setText(doObject.getPackageName());
                methodName.setText(doObject.getMethodName());
                callNumber.setText(String.valueOf(doObject.getCallNumber()));

                //Log.d(doObject.getDoi(),"开关状态******************"+doObject.getIsopen());
                desp.setText(doObject.getDesp());
               // CMDManager.callDOI(info,"1","chenyan1");
                CMDManager.doiInfo(doObject.getDoi());
                invoke.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HomeActivity context = mActivity;
                        try{
                            IBinder iBinder = context.conn.request(doObject.getPackageName(), "1");
                            IAPIBinder app = IAPIBinder.Stub.asInterface(iBinder);
                            Log.d("点击调用","     包名："+doObject.getPackageName()+"方法名"+doObject.getMethodName());
                            String result = app.callMethod(doObject.getMethodName(),"CommPoter");
                            invokeResult.setText(result);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                });
                final AlertDialog alertDialog = builder.create();
                alertDialog.setView(view);
                alertDialog.show();
                //mActivity.jumpToFragment(doiDetailFrament);
            }
        });
        return v;
    }


    public MySimpleAdapter(Activity activity, Context context, List<Map<String, Object>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        mActivity= (HomeActivity) activity;
        list=data;
        // TODO Auto-generated constructor stub
    }

}
