package ias.deepsearch.com.helper.ui.fragment;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;

import com.yancloud.android.contractclient.databaseHelper.DatabaseDao;
import com.yancloud.android.contractclient.databaseHelper.Do;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ias.deepsearch.com.helper.R;
import ias.deepsearch.com.helper.adapter.MySimpleAdapter;
import ias.deepsearch.com.helper.ui.activity.HomeActivity;


public class DoiFragment extends Fragment  {
  // List<String> metaData = new ArrayList<>();
    private HomeActivity mActivity;

    private static DatabaseDao databaseDao = DatabaseDao.getInstance();
    private ListView lv;

    private MySimpleAdapter simple_adapter;
    private SearchView searchView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.do_view, container, false);
        lv = (ListView) view.findViewById(R.id.list_view);

        //**********获取资源数据lyf
        Map<String, Integer> metaData = new HashMap<>();
        List<Map<String, Object>> list= new ArrayList<>();
        metaData=getData();
        for(String key:metaData.keySet())
        {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("link", key);
            map.put("button",metaData.get(key));
            list.add(map);
        }

        mActivity = (HomeActivity) getActivity();
        //适配器设置lyf
        simple_adapter=new MySimpleAdapter(mActivity,getActivity(), list, R.layout.list_item, new
                String[] {"link"}, new int[] {R.id.textView2});

//启用适配器
        lv.setAdapter(simple_adapter);
        return view;
    }


    private Map<String, Integer> getData() {
        List<Do> data  = databaseDao.getAllDoi();
       // List<Map<String, Integer>> list=new ArrayList<>();
        Map<String, Integer> map = new HashMap<String, Integer>();
        for(int i = 0;i<data.size();i++){
           // Log.d(data.get(i).getDoi(),"所有开关状态获取**********************"+data.get(i).getIsopen());
            map.put(data.get(i).getDoi(),data.get(i).getIsopen());
            //list.add(map);
        }
        return map;
    }






   /* @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.do_view,container,false);
        lv = view.findViewById(android.R.id.list);
        return inflater.inflate(R.layout.do_view,container,false);
    }

    //监听点击事件
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if(position == 0){
            Toast.makeText(getActivity(),"你点击了 消息中心",Toast.LENGTH_LONG).show();
        }else if(position == 1){
            Toast.makeText(getActivity(),"你点击了  家教记录",Toast.LENGTH_LONG).show();
        }else if(position == 2){
            Toast.makeText(getActivity(),"你点击了  设置",Toast.LENGTH_LONG).show();
        }
        super.onListItemClick(l, v, position, id);
    }


    //对数据进行加载
    private List<? extends Map<String, ?>> getData(int[] images, String[] names) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < images.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("img", images[i]);
            map.put("name", names[i]);
            list.add(map);
        }

        return list;
    }*/
}


