package ias.deepsearch.com.helper.util;


import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.socks.library.KLog;

//import im.fir.sdk.FIR;

/**
 * Created by vector on 16/4/17.
 */
public class IASApplication extends Application {


    public IASApplication() {
        super();
    }
    @Override
    protected  void attachBaseContext(Context base){
        super.attachBaseContext(base);
        android.support.multidex.MultiDex.install(this);

    }

    @Override
    public void onCreate() {
        super.onCreate();

      //  UpdateKey.API_TOKEN = "8bdc6797a1d6c974136047b21884640d";
     //   UpdateKey.APP_ID = "586e6d32ca87a80bf00006a2";
        Fresco.initialize(this);
    //    FIR.init(this);
        KLog.init(BuildConfig.LOG_DEBUG);

    }
}
