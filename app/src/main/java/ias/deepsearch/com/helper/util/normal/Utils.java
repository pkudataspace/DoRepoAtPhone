package ias.deepsearch.com.helper.util.normal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

import com.socks.library.KLog;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ias.deepsearch.com.helper.model.dp.AppInfo;

/**
 * Created by vector on 16/8/12.
 */
public class Utils {

    public static String buddha = "<html lang=\"zh-cn\"><head><meta charset=\"utf-8\"></head><body>#<pre>                            _ooOoo_<br>" +
            "#                           o8888888o<br>" +
            "#                           88\" . \"88<br>" +
            "#                           (| -_- |)<br>" +
            "#                            O\\ = /O<br>" +
            "#                        ____/`---'\\____<br>" +
            "#                      .   ' \\\\| |# `.<br>" +
            "#                       / \\\\||| : |||# \\<br>" +
            "#                     / _||||| -:- |||||- \\<br>" +
            "#                       | | \\\\\\ - #/ | |<br>" +
            "#                     | \\_| ''\\---/'' | |<br>" +
            "#                      \\ .-\\__ `-` ___/-. /<br>" +
            "#                   ___`. .' /--.--\\ `. . __<br>" +
            "#                .\"\" '< `.___\\_<|>_/___.' >'\"\".<br>" +
            "#               | | : `- \\`.;`\\ _ /`;.`/ - ` : | |<br>" +
            "#                 \\ \\ `-. \\_ __\\ /__ _/ .-` / /<br>" +
            "#         ======`-.____`-.___\\_____/___.-`____.-'======<br>" +
            "#                            `=---='<br>" +
            "#<br>" +
            "#         .............................................<br>" +
            "#                  佛祖保佑             永无BUG<br>" +
            "#          佛曰:<br>" +
            "#                  写字楼里写字间，写字间里程序员；<br>" +
            "#                  程序人员写程序，又拿程序换酒钱。<br>" +
            "#                  酒醒只在网上坐，酒醉还来网下眠；<br>" +
            "#                  酒醉酒醒日复日，网上网下年复年。<br>" +
            "#                  但愿老死电脑间，不愿鞠躬老板前；<br>" +
            "#                  奔驰宝马贵者趣，公交自行程序员。<br>" +
            "#                  别人笑我忒疯癫，我笑自己命太贱；<br>" +
            "#                  不见满街漂亮妹，哪个归得程序员？<br></pre></body></html>";

    public static boolean isEmpty(List l){
        if(l == null || l.size() == 0){
            return true;
        }
        return false;
    }


    public static boolean isNumeric(String str){
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if( !isNum.matches() ){
            return false;
        }
        return true;
    }


    public static String toURLEncoded(String paramString) {
        if (paramString == null || paramString.equals("")) {
            return "";
        }

        try
        {
            String str = new String(paramString.getBytes(), "utf-8");
            str = URLEncoder.encode(str, "utf-8");
            return str;
        }
        catch (Exception localException)
        {
        }

        return "";
    }

    public static List<AppInfo> getAppListInfo(Context context){
        List<AppInfo> appList = new ArrayList<AppInfo>(); //用来存储获取的应用信息数据

        List<PackageInfo> packages = context.getPackageManager().getInstalledPackages(0);

        for(int i=0;i<packages.size();i++) {
            PackageInfo packageInfo = packages.get(i);
            AppInfo tmpInfo =new AppInfo();
            tmpInfo.appName = packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString();
            tmpInfo.packageName = packageInfo.packageName;
            tmpInfo.versionName = packageInfo.versionName;
            tmpInfo.versionCode = packageInfo.versionCode;
            //Only display the non-system app info
            if((packageInfo.applicationInfo.flags& ApplicationInfo.FLAG_SYSTEM)==0)
            {
                appList.add(tmpInfo);//如果非系统应用，则添加至appList
            }

        }
        return appList;
    }


    public static void getString(String str, String regx) {
        //1.将正在表达式封装成对象Patten 类来实现
        Pattern pattern = Pattern.compile(regx);
        //2.将字符串和正则表达式相关联
        Matcher matcher = pattern.matcher(str);
        //3.String 对象中的matches 方法就是通过这个Matcher和pattern来实现的。

        KLog.v("liuyi", matcher.matches());
        //查找符合规则的子串
        while(matcher.find()){
            //获取 字符串
            KLog.v("liuyi", matcher.group());
            //获取的字符串的首位置和末位置
            KLog.v("liuyi", matcher.start()+"--"+matcher.end());
        }
    }


    // 得到本机ip地址
    public static String getLocalHostIp()
    {
        String ipaddress = "";
        try
        {
            Enumeration<NetworkInterface> en = null;
            en = NetworkInterface.getNetworkInterfaces();
            // 遍历所用的网络接口
            while (en.hasMoreElements())
            {
                NetworkInterface nif = en.nextElement();// 得到每一个网络接口绑定的所有ip
                Enumeration<InetAddress> inet = nif.getInetAddresses();
                // 遍历每一个接口绑定的所有ip
                while (inet.hasMoreElements())
                {
                    InetAddress ip = inet.nextElement();
                    if (!ip.isLoopbackAddress()
                            && ip instanceof Inet4Address)
                    {
                        ipaddress = ip.getHostAddress();
                    }
                }

            }
        }
        catch (SocketException e)
        {
            KLog.v("liuyi", "获取本地ip地址失败");
            e.printStackTrace();
        }
        return ipaddress;

    }
}
