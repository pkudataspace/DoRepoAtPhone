package ias.deepsearch.com.helper.receive;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.fastjson.JSONObject;
import com.socks.library.KLog;

import ias.deepsearch.com.helper.model.view_data.ViewTree;
import ias.deepsearch.com.helper.util.BuildConfig;
import ias.deepsearch.com.helper.util.normal.ActivityUtil;
import ias.deepsearch.com.helper.util.normal.SerializeUtil;
import ias.deepsearch.com.helper.util.normal.ViewUtil;

/**
 * Created by vector on 16/7/6.
 */
public class ActivityReceiver extends BroadcastReceiver{
    public static String myAction="ias.deepsearch.action";
    public static String myCmd = "ias.recover";
    public static String findView = "ias.findview";
    public static String getIntent = "ias.intent";
    public static String getViewTree = "ias.tree";


    public static String setIntent = "ias.setintent";

    Activity targetActivity;
    String activity_name;

    public ActivityReceiver(Activity activity) {
        targetActivity = activity;
        activity_name = activity.getClass().getName();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals(findView)) {
            if(!ActivityUtil.isBackground(targetActivity)) {
                final ViewUtil viewUtil = new ViewUtil(targetActivity);
                final String content = intent.getStringExtra("content");
                final int x = intent.getIntExtra("x", 0);
                final int y = intent.getIntExtra("y", 0);
                //KLog.v(BuildConfig.GETVIEW, "======find targeView with text:" + content + "======= in " + activity_name);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(!TextUtils.isEmpty(content))
                            viewUtil.findViewByContent(content);

                        if(x + y > 0){
                            viewUtil.findViewByPosition(x, y);
                        }
                    }
                }).start();

            }
        }else if(intent.getAction().equals(getIntent)){

                KLog.v(BuildConfig.GETINTENT, "======get Intent in==== " + activity_name);
                Intent intent1 = targetActivity.getIntent();
                KLog.v(BuildConfig.GETINTENT, SerializeUtil.serialize(intent1));
                Uri saveLogUri = Uri.parse("content://ias.deepsearch.com.helper.provider.DatasProvider/save_log");
                context.getContentResolver().query(saveLogUri, new String[]{activity_name, context.getPackageName()}, "ias_getintent", null, SerializeUtil.serialize(intent1));
        }else if(intent.getAction().equals(getViewTree)){

                KLog.v(BuildConfig.GETINTENT, "======get viewtree in==== " + activity_name);
                View root = targetActivity.getWindow().getDecorView();
                ViewTree viewTree = new ViewTree(context, root, context.getPackageName());
                Uri saveLogUri = Uri.parse("content://ias.deepsearch.com.helper.provider.DatasProvider/save_log");
                context.getContentResolver().query(saveLogUri, new String[]{activity_name, context.getPackageName()}, "ias_gettree", null, JSONObject.toJSONString(viewTree));
        }else if(intent.getAction().equals(setIntent)){

        }
    }



}
