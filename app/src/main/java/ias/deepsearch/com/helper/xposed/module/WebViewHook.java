package ias.deepsearch.com.helper.xposed.module;

import android.webkit.WebView;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by vector on 16/10/17.
 */

public class WebViewHook extends XC_MethodHook {

    XC_LoadPackage.LoadPackageParam loadPackageParam;

    public WebViewHook(XC_LoadPackage.LoadPackageParam loadPackageParam) {
        this.loadPackageParam = loadPackageParam;
    }

    @Override
    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
        // 打开webContentsDebuggingEnabled
        XposedHelpers.callStaticMethod(WebView.class, "setWebContentsDebuggingEnabled", true);
        XposedBridge.log("WebViewHook enabled: " + loadPackageParam.packageName);
    }
}
