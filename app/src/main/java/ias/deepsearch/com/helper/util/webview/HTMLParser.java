package ias.deepsearch.com.helper.util.webview;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by vector on 16/8/9.
 */
public class HTMLParser {
    Document doc;

    public HTMLParser (String html, String charset) {
        doc = Jsoup.parse(html, charset);
    }

    public String getTextBySelector (String selector) {
        Elements targets = doc.select(selector);
        StringBuilder sb =new StringBuilder();
        for (Element t : targets) {
            for (Element e : t.getAllElements()) {
                sb.append(e.text());
            }
        }
        return sb.toString();
    }
}
