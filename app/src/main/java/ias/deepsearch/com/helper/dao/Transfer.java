package ias.deepsearch.com.helper.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


/**
 * Created by vector on 16/7/11.
 */
public class Transfer{

    public static String TABLE_NAME = "TRANSFER";
    public static String COLUMN_ID = "id";
    public static String COLUMN_NOW = "now";
    public static String COLUMN_LAUNCHACTIVITY = "launch_activity";
    public static String COLUMN_TARGETACTIVITY = "target_activity";
    public static String COLUMN_INTENTSER = "intent_ser";


    private int now;
    private String launchActivity;
    private String targetActivity;
    private String intentSer;


    public int getNow() {
        return now;
    }

    public void setNow(int now) {
        this.now = now;
    }

    public String getLaunchActivity() {
        return launchActivity;
    }

    public void setLaunchActivity(String launchActivity) {
        this.launchActivity = launchActivity;
    }

    public String getIntentSer() {
        return intentSer;
    }

    public void setIntentSer(String intentSer) {
        this.intentSer = intentSer;
    }

    public String getTargetActivity() {
        return targetActivity;
    }

    public void setTargetActivity(String targetActivity) {
        this.targetActivity = targetActivity;
    }

    public Transfer() {
        this.now = -1;
        this.launchActivity = "";
        this.targetActivity = "";
        this.intentSer = "";
    }

    public Transfer(int now, String launchActivity, String intentSer) {
        this.now = now;
        this.launchActivity = launchActivity;
        this.targetActivity = "";
        this.intentSer = intentSer;
    }

    public Transfer(int now, String launchActivity, String targetActivity, String intentSer) {
        this.now = now;
        this.launchActivity = launchActivity;
        this.targetActivity = targetActivity;
        this.intentSer = intentSer;
    }

    public Transfer get(SQLiteDatabase db){
        Cursor cursor = db.query(Transfer.TABLE_NAME, null, null, null, null, null, null);
        Transfer transfer = null;
        if(cursor != null && cursor.moveToFirst()){
            transfer = new Transfer();
            transfer.setNow(cursor.getInt(cursor.getColumnIndex("now")));
            transfer.setLaunchActivity(cursor.getString(cursor.getColumnIndex("launch_activity")));
            transfer.setTargetActivity(cursor.getString(cursor.getColumnIndex("target_activity")));
            transfer.setIntentSer(cursor.getString(cursor.getColumnIndex("intent_ser")));
        }

        cursor.close();
        return transfer;
    }


    public void save(SQLiteDatabase db){
        Cursor cursor = db.query(Transfer.TABLE_NAME, null, null, null, null, null, null);
        if(cursor != null && cursor.moveToFirst()) {
            //TODO update
            ContentValues cv = new ContentValues();
            cv.put("now", now);
            cv.put("launch_activity", launchActivity);
            cv.put("target_activity", targetActivity);
            cv.put("intent_ser", intentSer);
            db.update(Transfer.TABLE_NAME, cv, null, null);
            cursor.close();

        }else{
            //TODO insert
            ContentValues cv = new ContentValues();
            cv.put("now", now);
            cv.put("launch_activity", launchActivity);
            cv.put("target_activity", targetActivity);
            cv.put("intent_ser", intentSer);
            db.insert(Transfer.TABLE_NAME, null, cv);
        }
    }

}
