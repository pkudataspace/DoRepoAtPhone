package ias.deepsearch.com.helper.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ias.deepsearch.com.helper.util.normal.ContextHolder;
import ias.deepsearch.com.helper.util.normal.FloatWindowManager;


/**
 * Created by vector on 16/7/21.
 */
public class FloatWindowService extends Service{
    /**
     * 用于在线程中创建或移除悬浮窗。
     */
    private Handler handler = new Handler();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent == null)
            return super.onStartCommand(intent, flags, startId);

        boolean show = intent.getBooleanExtra("show", true);
        final String message = intent.getStringExtra("message");

        if(!show){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(FloatWindowManager.isWindowShowing())
                        FloatWindowManager.removeSmallWindow(getApplicationContext());
                }
            });
            return super.onStartCommand(intent, flags, startId);
        }else{
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(!FloatWindowManager.isWindowShowing())
                        FloatWindowManager.createSmallWindow(getApplicationContext());
                    FloatWindowManager.updateUsedPercent(getApplicationContext(), message);
                }
            });
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class RefreshTask extends TimerTask {

        @Override
        public void run() {
            // 当前没有悬浮窗显示，则创建悬浮窗。
            if (!FloatWindowManager.isWindowShowing()) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        FloatWindowManager.createSmallWindow(getApplicationContext());
                    }
                });
            } else if (FloatWindowManager.isWindowShowing()) {
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        FloatWindowManager.updateUsedPercent(getApplicationContext());
//                    }
//                });
            }
        }

    }


    public static void updateFloatWindow(String message,boolean isShow){
        Context applicationContent = ContextHolder.getContext();
        Intent intent = new Intent(applicationContent, FloatWindowService.class);
        intent.putExtra("show", isShow);
        intent.putExtra("message", message);
        applicationContent.startService(intent);
    }

}
