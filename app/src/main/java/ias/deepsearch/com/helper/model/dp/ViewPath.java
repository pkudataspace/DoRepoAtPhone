package ias.deepsearch.com.helper.model.dp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vector on 16/7/31.
 */
public class ViewPath {
    @SerializedName("xpath")@Expose
    public String xpath;
    @SerializedName("resource_id")@Expose
    public String resource_id;
}
