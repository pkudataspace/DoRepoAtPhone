package ias.deepsearch.com.helper.xposed;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.webkit.WebView;


import com.yancloud.android.reflection.YanCloudStub;

import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Scanner;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import ias.deepsearch.com.helper.receive.ActivityReceiver;
import ias.deepsearch.com.helper.xposed.module.ActivityOnCreateHook;
import ias.deepsearch.com.helper.xposed.module.VMHelper;
import ias.deepsearch.com.helper.xposed.module.VisiableManager;
import ias.deepsearch.com.helper.xposed.module.WebViewHook;

/**
 * Created by root on 7/1/17.
 */

public class ActivityThreadHook implements IXposedHookZygoteInit, IXposedHookLoadPackage {
    static final String TAG = "ActivityThreadHook";
    private XC_MethodHook callback;
    private static  boolean activatedZygote = false;


    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        try {
            FileInputStream fin = new FileInputStream("/sdcard/dumpdex/xposeconf.txt");
            Scanner sc = new Scanner(fin);
            String pkgName = sc.nextLine();
            if (!pkgName.equals(loadPackageParam.processName))
                return;
            boolean collectArg = Boolean.valueOf(sc.nextLine());
            callback = MethodCollector.getInstance(loadPackageParam.processName,loadPackageParam.classLoader,collectArg);
            for (;sc.hasNextLine();){
                hookClz(loadPackageParam.classLoader,sc.nextLine());
            }
          }catch(Exception e){
            Log.d(TAG,"clz:com.android.server.wm.WindowManagerService not found, just exist!");
        }
    }

    private void hookClz(ClassLoader classLoader, String s) {
        try{
            Class c = classLoader.loadClass(s);
            for (Method m:c.getDeclaredMethods()){
                XposedBridge.hookMethod(m,callback);
            }
            for (Constructor m:c.getDeclaredConstructors()){
                XposedBridge.hookMethod(m,callback);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        if (!activatedZygote)
            return;
        Class<?> bind = Class.forName("android.app.ActivityThread$AppBindData");
        XposedHelpers.findAndHookMethod("android.app.ActivityThread", null,
                "handleBindApplication", bind, new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                    }

                    protected void afterHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
                        Log.d(TAG, "after bindApplication! "+param.thisObject.getClass().getCanonicalName());
                        Field mApplication = param.thisObject.getClass().getDeclaredField("mInitialApplication");
                        mApplication.setAccessible(true);
                        Context c = (Context) mApplication.get(param.thisObject);
                        YanCloudStub.init(c);
                    }
                });

        XposedHelpers.findAndHookMethod("android.app.Activity", null,
                "onResume", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        YanCloudStub.onResume((Context)param.thisObject);
                    }
                });

    }
}

