package ias.deepsearch.com.helper.util.normal;

/**
 * Created by vector on 16/8/9.
 */
public class Constants {
    public static final String NORMAL_TAG = "liuyi";
    public static final String WEB_TAG = "liuyi-webelement";
    public static final String LOCAL_IP = "127.0.0.1";
    //存放本地引擎的位置
    public static final String LOCAL_ENGINE_PATH = "/sdcard/apiminier";
    //当前支持的5个app的包名
    public static final String[] packages = {"me.ele", "com.sankuai.meituan", "com.jingdong.app.mall", "com.yongche.android", "com.sdu.didi.psnger"};
}
