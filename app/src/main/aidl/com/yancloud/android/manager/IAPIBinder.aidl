// IConnecter.aidl
package com.yancloud.android.manager;
import android.os.Parcel;
interface IAPIBinder {
    List<String> getMethodNames();
    String callMethod(String method,String arg);
}