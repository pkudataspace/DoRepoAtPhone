package com.yancloud.android.manager;
import android.os.Parcel;
import com.yancloud.android.manager.PackageVersionPair;

interface IAppConnector {
    void register(String pkgName,String version, IBinder binder);
    IBinder request(String pkgName,String version);
    List<PackageVersionPair> listPackages();    //看有多少APP
}